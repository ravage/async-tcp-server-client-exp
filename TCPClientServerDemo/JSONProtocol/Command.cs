using System;
using ProtocolContract;

namespace JSONProtocol
{
	public class Command: ICommand
	{
		private Commands directive;
		private String payload;


		public Commands Directive {
			get {
				return directive;
			}
			set {
				directive = value;
			}
		}

		public String Payload {
			get {
				return payload;
			}
			set {
				payload = value;
			}
		}
	}
}

