using System;
using System.Text;
using Newtonsoft.Json;
using ProtocolContract;

namespace JSONProtocol
{
	public class Protocol: IProtocol
	{
        private StringBuilder data;
        private bool packetComplete;

        public Protocol() {
            data = new StringBuilder();
        }

        public byte[] Encode(ICommand command)
        {
            string serializedCommand = JsonConvert.SerializeObject(command);
            return Encoding.UTF8.GetBytes(serializedCommand);
        }

        public ICommand Decode(string payload)
        {
            Command command = null;
            packetComplete = true;

            data.Append(payload);

            try
            {
                command = JsonConvert.DeserializeObject<Command>(data.ToString());
                data.Clear();
            }
            catch (JsonReaderException) {
                packetComplete = false;
            }

            return command;
        }

        public bool PacketComplete {
            get { return packetComplete; }
        }
    }
}

