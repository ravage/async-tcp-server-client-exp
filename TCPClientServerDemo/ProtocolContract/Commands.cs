using System;

namespace ProtocolContract
{
    public enum Commands : int
    {
        List,
        Broadcast,
        Init
    }
}

