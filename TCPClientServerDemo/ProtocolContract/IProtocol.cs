using System;

namespace ProtocolContract
{
	public interface IProtocol
	{
		byte[] Encode(ICommand command);
		ICommand Decode(String payload);
        bool PacketComplete { get;  }
	}
}

