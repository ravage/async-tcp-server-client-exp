﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProtocolContract
{
    public interface ICommand
    {
        Commands Directive { get; set; }
        string Payload { get; set; }
    }
}
