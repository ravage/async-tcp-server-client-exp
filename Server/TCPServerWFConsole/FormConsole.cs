﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LibTCPServer;
using System.Net;
using System.Diagnostics;

namespace TCPServerWFConsole
{
    public partial class FormConsole : Form
    {
        TCPServer server;
        BindingList<IPEndPoint> clients;

        public FormConsole()
        {
            InitializeComponent();

            server = new TCPServer();
            clients = new BindingList<IPEndPoint>();
  
            server.OnClientAccepted += OnClientAcceptedHandler;
            server.OnClientDisconnected += OnClientDisconnectedHandler;
            server.OnMessageReceived += OnMessageReceivedHandler;
            server.OnMessageSent += OnMessageSentHandler;
            server.OnDisconnectComplete += OnDisconnectCompleteHandler;

            lstClients.DataSource = clients;
        }

        void OnMessageSentHandler(ConnectionState state)
        {
            Debug.WriteLine("OnMessageSent");
        }

        void OnMessageReceivedHandler(ConnectionState state)
        {
            Debug.WriteLine("OnMessageReceived");
            IPEndPoint endpoint = state.Socket.RemoteEndPoint as IPEndPoint;
            Log(String.Format("Received message from client {0}", endpoint.Address));
        }

        void OnClientDisconnectedHandler(IPEndPoint endpoint)
        {
            Log(String.Format("Client {0} disconnected", endpoint.Address));
            RemoveClient(endpoint);
        }

        void OnClientAcceptedHandler(ConnectionState state)
        {
            IPEndPoint endpoint = state.Socket.RemoteEndPoint as IPEndPoint;
            Log(String.Format("Client {0} connected", endpoint.Address));
            AddClient(endpoint);
        }

        void OnDisconnectCompleteHandler()
        {
            Log("Server disconnect complete");

            btnDisconnect.Enabled = false;
            btnListen.Enabled = true;
        }

        private void FormConsole_Load(object sender, EventArgs e)
        {
            lvLogs.Columns[0].Width = 140;
            lvLogs.Columns[1].Width = lvLogs.Width - (lvLogs.Columns[0].Width + 30);

            btnDisconnect.Enabled = false;
        }

        private void btnListen_Click(object sender, EventArgs e)
        {
            server.Start();
            
            btnListen.Enabled = false;
            btnDisconnect.Enabled = true;

            Log("Waiting for connections");
        }

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            Log("Server disconnecting, notifying clients");
            server.Stop();
        }


        private void Log(string message)
        {
            lvLogs.Invoke((Action) delegate {
                ListViewItem item = new ListViewItem();
                item.Text = DateTime.Now.ToString("dd/MM/yyy @ HH:MM:ss");
                item.SubItems.Add(message);
                lvLogs.Items.Add(item); 
                lvLogs.EnsureVisible(lvLogs.Items.Count - 1);
            }); 
        }

        private void AddClient(IPEndPoint endpoint) {
            lstClients.BeginInvoke((Action) delegate
            {
                clients.Add(endpoint);
            });
        }

        private void RemoveClient(IPEndPoint endpoint)
        {
            lstClients.BeginInvoke((Action)delegate
            {
                clients.Remove(endpoint);
            });
        }
    }
}
