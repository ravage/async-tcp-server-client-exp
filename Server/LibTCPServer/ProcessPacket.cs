﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtocolContract;
using JSONProtocol;
using System.Net;
using Newtonsoft.Json;

namespace LibTCPServer
{
    class ProcessPacket
    {
        private ICommand command;
        private TCPServer server;
        private ConnectionState state;

        public ProcessPacket(TCPServer server, ConnectionState state, ICommand command)
        {
            this.command = command;
            this.server = server;
            this.state = state;

            Process();
        }

        private void Process()
        {
            if (command.Directive == Commands.Broadcast)
            {
                ProcessBroadcast();
            }
            else if (command.Directive == Commands.List)
            {
                ProcessList();
            }
        }

        private void ProcessList()
        {
            List<String> endpoints = new List<string>();
            ICommand command = new Command();

            foreach (ConnectionState client in server.ConnectedClients)
            {   
                IPEndPoint endpoint = client.Socket.RemoteEndPoint as IPEndPoint;
                endpoints.Add(String.Format("{0}:{1}", endpoint.Address.ToString(), endpoint.Port));
            }

            command.Directive = Commands.List;
            command.Payload = JsonConvert.SerializeObject(endpoints);
            state.Socket.Send(state.Protocol.Encode(command));  
        }

        private void ProcessBroadcast()
        {
            foreach (ConnectionState connection in server.ConnectedClients)
            {
                connection.Socket.Send(connection.Protocol.Encode(command));
            }
        }
    }
}
