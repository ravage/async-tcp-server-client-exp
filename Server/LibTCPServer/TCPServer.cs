using System;
using System.Net.Sockets;
using System.Net;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel.Channels;
using System.Threading;
using JSONProtocol;
using ProtocolContract;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace LibTCPServer
{
	public class TCPServer
	{
		public event Action<ConnectionState> OnClientAccepted;
		public event Action<ConnectionState> OnMessageReceived;
		public event Action<ConnectionState> OnMessageSent;
		public event Action<IPEndPoint> OnClientDisconnected;
        public event Action OnDisconnectComplete;

		private Socket server;
		private List<ConnectionState> clients;
		private BufferManager buffers;
        private bool isDisconnecting;

        private const int bufferSize = 1024 * 1024; // 1MB
        private const int bufferUnit = 1; // 128KB

		public TCPServer()
		{
			clients = new List<ConnectionState>();
			buffers = BufferManager.CreateBufferManager(bufferSize, bufferUnit);
            isDisconnecting = false;
		}

		public void Start()
		{
            isDisconnecting = false;

            server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			server.Bind(new IPEndPoint(IPAddress.Any, 8090));
			server.Listen(1024);

			WaitForConnections();
		}

        public void Stop()
        {
            isDisconnecting = true;

            foreach (ConnectionState state in clients)
            {
                state.Socket.Close();
            }

            server.Close();

            clients.Clear();

            if (OnDisconnectComplete != null)
            {
                OnDisconnectComplete();
            }
        }

        public ReadOnlyCollection<ConnectionState> ConnectedClients
        {
            get { return clients.AsReadOnly(); }
        }

		private void WaitForConnections()
		{
			server.BeginAccept(OnClientAccept, new ConnectionState(new Protocol()));
		}

		private void OnClientAccept(IAsyncResult result)
		{
            if (isDisconnecting)
            {
                return;
            }

			ConnectionState state = result.AsyncState as ConnectionState;

			state.Socket = server.EndAccept(result);

			clients.Add(state);

			Send(state);

			WaitForConnections();
            
            if (OnClientAccepted != null)
            {
                OnClientAccepted(state);
            }
		}

		private void Send(ConnectionState state)
		{
			Command command = new Command();
			command.Directive = Commands.Init;
			command.Payload = "Init!";

			state.Buffer = state.Protocol.Encode(command);
			state.Socket.BeginSend(state.Buffer, 0, state.Buffer.Length, SocketFlags.None, OnSendComplete, state);
		}

		private void OnSendComplete(IAsyncResult result)
		{
			ConnectionState state = result.AsyncState as ConnectionState;
			state.Socket.EndSend(result);

			Read(state);

            if (OnMessageSent != null)
            {
                OnMessageSent(state);
            }
		}

		private void Read(ConnectionState state)
		{
			state.Buffer = buffers.TakeBuffer(bufferUnit);
			state.Socket.BeginReceive(state.Buffer, 0, state.Buffer.Length, SocketFlags.None, OnReadComplete, state);
		}

		private void OnReadComplete(IAsyncResult result)
		{
            if (isDisconnecting) {
                return;
            }

			ConnectionState state = result.AsyncState as ConnectionState;

            try
            {
                int bytesRead = state.Socket.EndReceive(result);

                if (bytesRead > 0)
                {
                    ICommand command = state.Protocol.Decode(Encoding.UTF8.GetString(state.Buffer, 0, bytesRead));
                    buffers.ReturnBuffer(state.Buffer);

                    Read(state);

                    if (state.Protocol.PacketComplete) {
                        if (OnMessageReceived != null)
                        {
                            OnMessageReceived(state);
                        }

                        //ProcessRequest(command);
                        Task.Factory.StartNew(() => new ProcessPacket(this, state, command));
                    }
                }
                else
                {
                    if (!isDisconnecting)
                    {
                        DisposeConnection(state);
                    }
                }
            }
            catch (SocketException)
            {
                DisposeConnection(state);
            }
		}

        private void OnDisconnect(IAsyncResult result) {
            ConnectionState state = result.AsyncState as ConnectionState;
            state.Socket.EndDisconnect(result);
        }

        private void DisposeConnection(ConnectionState state) {
            IPEndPoint endpoint = state.Socket.RemoteEndPoint as IPEndPoint;
            state.Socket.Close();
            buffers.ReturnBuffer(state.Buffer);
            clients.Remove(state);

            if (OnClientDisconnected != null)
            {
                OnClientDisconnected(endpoint);
            }
        }
    }
}

