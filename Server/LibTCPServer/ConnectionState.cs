using System;
using System.Net.Sockets;
using ProtocolContract;

namespace LibTCPServer
{
	public class ConnectionState
	{
		private Socket socket;
		private byte[] buffer;
		private IProtocol protocol;

		public ConnectionState(IProtocol protocol) {
			this.protocol = protocol;
		}

		public Socket Socket {
			get {
				return socket;
			}
			set {
				socket = value;
			}
		}

		public byte[] Buffer {
			get {
				return buffer;
			}

			set {
				buffer = value;
			}
		}

		public IProtocol Protocol {
			get {
				return protocol;
			}
			set {
				protocol = value;
			}
		}
	}
}

