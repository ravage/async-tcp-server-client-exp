﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LibTCPClient;
using System.Net;
using ProtocolContract;
using Newtonsoft.Json;

namespace TCPClientWFConsole
{
    public partial class FormConsole : Form
    {
        TCPClient client;
        BindingList<string> clients;
        

        public FormConsole()
        {
            InitializeComponent();

            client = new TCPClient("127.0.0.1", 8090);
            clients = new BindingList<string>();

            client.OnConneced += OnConnectedHandler;
            client.OnConnectFailed += OnConnectFailedHandler;
            client.OnMessageReceived += OnMessageReceivedHandler;
            client.OnPacketProcessed += OnPacketProcessedHandler;
            client.OnException += OnExceptionHandler;
            client.OnRemoteDisconnect += OnRemoteDisconnectHandler;


            lstClients.DataSource = clients;

            btnDisconnect.Enabled = false;
            btnSendBroadcast.Enabled = false;
            btnRequestList.Enabled = false;
        }

        private void OnConnectedHandler(IPEndPoint endpoint) {
            Log(String.Format("Connected to {0}:{1}", endpoint.Address, endpoint.Port));

            this.BeginInvoke((Action)delegate
            {
                btnConnect.Enabled = false;
                btnRequestList.Enabled = true;
                btnSendBroadcast.Enabled = true;
                btnDisconnect.Enabled = true;
            });
        }

        private void OnConnectFailedHandler(IPEndPoint endpoint) {
            Log(String.Format("Connection to {0}:{1} failed", endpoint.Address, endpoint.Port));
        }

        private void OnMessageReceivedHandler(ICommand message)
        {
            Log(String.Format("Directive: {0} :: Payload: {1}", message.Directive, message.Payload));

            HandleMessage(message);
        }

        private void OnExceptionHandler(string message) {
            Log(String.Format("Exception: {0}", message));
        }

        private void HandleMessage(ICommand message)
        {
            switch (message.Directive)
            {
                case Commands.List:
                    HandleDirectiveList(message.Payload);
                    break;
                case Commands.Broadcast:
                    break;
                default:
                    break;
            }
        }

        private void HandleDirectiveList(string payload)
        {
            var endpoints = JsonConvert.DeserializeObject<List<string>>(payload);
            clients.Clear();

            lstClients.BeginInvoke((Action)delegate
            {
                foreach (var item in endpoints)
                {
                    clients.Add(item);
                }                
            });
        }

        private void OnRemoteDisconnectHandler() {
            DisconnectedButtonState();  
        }

        private void OnPacketProcessedHandler()
        {
            Log(String.Format("Message: Packet Processed"));
        }

        private void Log(string message) {
            lvLogs.BeginInvoke((Action)delegate
            {
                ListViewItem item = new ListViewItem();
                item.Text = DateTime.Now.ToString("dd/MM/yyy @ HH:MM:ss");
                item.SubItems.Add(message);
                lvLogs.Items.Add(item);
                lvLogs.EnsureVisible(lvLogs.Items.Count - 1);
            });
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            client.Connect();
        }

        private void FormConsole_Load(object sender, EventArgs e)
        {
            lvLogs.Columns[0].Width = 140;
            lvLogs.Columns[1].Width = lvLogs.Width - (lvLogs.Columns[0].Width + 30);
            
            btnDisconnect.Enabled = false;
        }

        private void btnSendBroadcast_Click(object sender, EventArgs e)
        {
            client.Broadcast(txtBroadcastMessage.Text);
        }

        private void btnRequestList_Click(object sender, EventArgs e)
        {
            client.RequestClients();
        }

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            client.Disconnect();

            DisconnectedButtonState();
        }

        private void DisconnectedButtonState() {
            this.BeginInvoke((Action)delegate
            {
                btnSendBroadcast.Enabled = false;
                btnRequestList.Enabled = false;
                btnDisconnect.Enabled = false;
                btnConnect.Enabled = true;
            });
        }
    }
}
