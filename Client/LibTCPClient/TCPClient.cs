﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;
using JSONProtocol;
using ProtocolContract;
using System.Threading.Tasks;

namespace LibTCPClient
{
    public class TCPClient
    {
        public event Action<IPEndPoint> OnConneced;
        public event Action<IPEndPoint> OnConnectFailed;
        public event Action OnMessageSent;
        public event Action<String> OnMessageFailure;
        public event Action<ICommand> OnMessageReceived;
        public event Action OnPacketProcessed;
        public event Action<string> OnException;
        public event Action OnRemoteDisconnect;

        private const int bufferSize = 1024 * 128; // 128KB

        private Socket client;
        private byte[] buffer;
        private EndPoint endpoint;
        private IProtocol protocol;

        public TCPClient(string address, int port)
        {
            buffer = new byte[bufferSize];
            endpoint = new IPEndPoint(IPAddress.Parse(address), port);
            protocol = new Protocol();
        }

        public void Connect() {
            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            client.BeginConnect(endpoint, OnConnect, client);
        }

        private void OnConnect(IAsyncResult result) {
            Socket state = result.AsyncState as Socket;

            try
            {
                state.EndConnect(result);

                if (OnConneced != null) {
                    OnConneced((IPEndPoint)endpoint);
                }

                Receive(state);
            }
            catch (SocketException e) {
                if (OnConnectFailed != null)
                {
                    OnConnectFailed((IPEndPoint)endpoint);
                }
            }
        }

        private void Send(byte[] data) {
            client.BeginSend(data, 0, data.Length, SocketFlags.None, OnSendComplete, client);
        }

        private void OnSendComplete(IAsyncResult result)
        {
            Socket state = result.AsyncState as Socket;

            try
            {
                int bytesWritten = state.EndSend(result);

                if (OnMessageSent != null)
                {
                    OnMessageSent();
                }

                Receive(state);
            }
            catch (SocketException e) {
                if (OnMessageFailure != null) { 
                    OnMessageFailure(e.ErrorCode.ToString());
                }
            }
        }

        private void Receive(Socket state)
        {
            Array.Clear(buffer, 0, buffer.Length);

            state.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, OnReveiceComplete, state);
        }

        private void OnReveiceComplete(IAsyncResult result) {
            Socket state = result.AsyncState as Socket;

            try
            {
                int bytesRead = state.EndReceive(result);

                if (bytesRead > 0)
                {

                    ICommand command = protocol.Decode(Encoding.UTF8.GetString(buffer, 0, bytesRead));

                    if (OnMessageReceived != null)
                    {
                        OnMessageReceived(command);
                    }

                    Receive(state);
                }
                else {
                    Disconnect();
                }

            }
            catch (SocketException e)
            {
                if (OnException != null) {
                    OnException(e.Message);
                }

                Disconnect();
            }
            catch (InvalidOperationException) { 
            
            }
        }

        public void Send(string message)
        {
            throw new NotImplementedException();
        }

        public void Broadcast(string message)
        {
            ICommand command = new Command();
            command.Directive = Commands.Broadcast;
            command.Payload = message;
            Send(protocol.Encode(command));
        }

        public void RequestClients()
        {
            ICommand command = new Command();
            command.Directive = Commands.List;
            Send(protocol.Encode(command));
        }

        public void Disconnect()
        {
            client.Shutdown(SocketShutdown.Both);
            client.Close();

            if (OnRemoteDisconnect != null) {
                OnRemoteDisconnect();
            }
         }
    }
}
