﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtocolContract;
using JSONProtocol;
using System.Net;
using Newtonsoft.Json;

namespace LibTCPClient
{
    class ProcessPacket
    {
        private ICommand command;
        private TCPClient client;

        public ProcessPacket(TCPClient client, ICommand command)
        {
            this.command = command;
            this.client = client;
        }
    }
}
